unit idocbuilder;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

type

    TFormatFlags = set of (ffBold,ffItalic,ffUnderlined,ffRight,ffCenter,ffLeft);

  { IADocBuilder }

  IADocBuilder = interface
     ['{BE20ADCB-18DE-4350-BB2D-9898E2847CC3}']
     // StartDoc - передается название для документа, которое будет положено
     // в основу имени файла докемента и папки для содержимого html
     procedure StartDoc(name: string);
     // AddHeader - передается текст заголовка и флаги форматирования
     procedure AddHeader(text: string; formatflags: TFormatFlags);
     // AddText - передается текст абзаца и флаги форматирования
     procedure AddText(text: string; formatflags: TFormatFlags);
     // StartTable - передается массив с заголовками для построения таблицы,
     // по количеству заголовков определяется и количество столбцов,
     // выравнивание по умолчанию - по содержимому внутри и по всей ширине для таблицы
     procedure StartTable(headers: array of string);
     // AddRow - передается массив констант для наполнения строки таблицы,
     // количество должно совпадать с количеством столбцов, если меньше - то
     // оставшиеся будут пустыми, превышающие - отсекутся
     procedure AddRow(const values: array of const);
     // AddRowHeader - передается текст заголовка внутри таблицы, заголовок
     // помещается в строку с объединенными ячейками
     procedure AddRowHeader(text: string; formatflags: TFormatFlags);
     // возвращается true при успешном закрытии таблицы, false - если таблица и
     // не строилась
     function FinishTable: boolean;
     // возвращается имя файла с путем, в котором сохранен документ, либо '' при неудаче
     function FinishDoc: string;
     // возвразщается true если документ готов к генерации
     function Ready: boolean;
  end;

implementation

end.

