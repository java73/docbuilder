unit c_htmlbuilder;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LazFileUtils, graphics
  , idocbuilder
  ;

const
  MaxImgHeight = 150;    // максимальная высота картинки в таблице

type

  { THTMLDocBuilder }

  THTMLDocBuilder = class(TInterfacedObject, IADocBuilder)
  private
    filename: string;
    foldername: string;
    isTableOpened: integer;
    isReady: boolean;
    Doc: TStringList;
    ImgCount: integer;
    function CombineLine(text: string; formatflags: TFormatFlags): string;
  public
    procedure StartDoc(name: string);
    procedure AddHeader(text: string; formatflags: TFormatFlags);
    procedure AddText(text: string; formatflags: TFormatFlags);
    procedure StartTable(headers: array of string);
    procedure AddRow(const values: array of const);
    procedure AddRowHeader(text: string; formatflags: TFormatFlags);
    function FinishTable: boolean;
    function FinishDoc: string;
    function Ready: boolean;
    constructor Create(name: string);
    destructor Destroy; override;
  end;

implementation

{ THTMLDocBuilder }

function THTMLDocBuilder.CombineLine(text: string; formatflags: TFormatFlags
  ): string;
var
  b_str: string = '';
  e_str: string = '';
  t_str: string = '';
begin
     if (ffBold in formatflags) then
         begin
           b_str += '<b>';
           e_str := '</b>'+e_str;
         end;
     if (ffItalic in formatflags) then
         begin
           b_str += '<i>';
           e_str := '</i>'+e_str;
         end;
     if (ffUnderlined in formatflags) then
         begin
           b_str += '<u>';
           e_str := '</u>'+e_str;
         end;
     if (ffCenter in formatflags) then
         t_str := 'center'
     else if (ffRight in formatflags) then
         t_str := 'right'
     else if (ffLeft in formatflags) then
         t_str := 'left';
     b_str += #13#10 + '<p';
     if t_str<>'' then b_str += 'align="'+t_str+'"';
     b_str += '>';
     e_str := '</p>' + #13#10+ e_str;
     Result := b_str + text + e_str;
end;

procedure THTMLDocBuilder.StartDoc(name: string);
var
  Style: TStringList;
begin
     Doc.Clear;
     Doc.Add('<html>');
     Doc.Add('<head><meta charset="utf-8">');
     Doc.Add('<title>'+name+'</title>');
     if FileExists('style.template') then
         begin
            Doc.Add('<style type="text/css">');
            Style := TStringList.Create;
            Style.LoadFromFile('style.template');
            Doc.Add(Style.Text);
            Doc.Add('</style>');
            Style.Free;
         end;
     Doc.Add('</head>');
     Doc.Add('<body>');
end;

procedure THTMLDocBuilder.AddHeader(text: string; formatflags: TFormatFlags);
begin
     Doc.Add('<h2>'+CombineLine(text,formatflags)+#13#10+'</h2>');
end;

procedure THTMLDocBuilder.AddText(text: string; formatflags: TFormatFlags);
begin
     Doc.Add(CombineLine(text,formatflags));
end;

procedure THTMLDocBuilder.StartTable(headers: array of string);
var
  i: integer;
begin
     if Length(headers)<=0 then exit;
     isTableOpened := Length(headers);
     Doc.Add('<table align="center">');
     Doc.Add('<tbody>'+#13#10+'<tr>');
     for i := 0 to isTableOpened-1 do
         Doc.Add('<th>'+CombineLine(headers[i],[ffBold])+'</th>'+#13#10);
     Doc.Add('</tr>'+#13#10);
end;

procedure THTMLDocBuilder.AddRow(const values: array of const);
var
  t_str: string = '';
  g_str: string;
  i: integer = 0;
  img_filename: string;
begin
     if (isTableOpened<=0) then exit;
     Doc.Add('<tr>'+#13#10);
     while (i<isTableOpened) do
         begin
            if (i=High(values)) then
              t_str := '<td colspan="'+IntToStr(isTableOpened-i)+'">'
            else t_str := '<td>';
            if (values[i].VType = vtObject) then
              begin
                if (values[i].VObject is TJPEGImage) then
                   begin
                    img_filename := foldername + PathDelim + 'image'+
                                    IntToStr(ImgCount) + '.jpg';
                    (values[i].VObject as TJPEGImage).SaveToFile(img_filename);
                    t_str += '<img src="'+img_filename+'"';
                    if ((values[i].VObject as TJPEGImage).Height>MaxImgHeight) then
                       t_str+= 'height="'+IntToStr(MaxImgHeight)+'"';
                    t_str += '>';
                    Inc(ImgCount);
                  end;
              end
            else
              begin
                 if (values[i].VType = vtAnsiString)then
                   g_str := string(values[i].VAnsiString)
                 else g_str :='неопознанный объект ';
                 t_str += CombineLine(g_str,[]);
              end;
            t_str += '</td>'+#13#10;
            Doc.Add(t_str);
            Inc(i);
            if (i=Length(values)) then break;
         end;
     Doc.Add('</tr>'+#13#10);
end;

procedure THTMLDocBuilder.AddRowHeader(text: string; formatflags: TFormatFlags);
begin
  if IsTableOpened>0 then
    begin
      Doc.Add('<tr><th colspan="'+IntToStr(IsTableOpened)+'">');
      AddHeader(text,formatflags);
      Doc.Add('</th></tr>'+#13#10);
    end;
end;

function THTMLDocBuilder.FinishTable: boolean;
begin
     if isTableOpened>0 then
       begin
         Doc.Add('</tbody></table>');
         IsTableOpened := 0;
         Result := true;
       end
     else Result := false;
end;

function THTMLDocBuilder.FinishDoc: string;
begin
     if (isTableOpened>0) then FinishTable;
     Doc.Add('</body>');
     Doc.Add('</html>');
     Doc.SaveToFile(filename);
     Result := filename;
end;

function THTMLDocBuilder.Ready: boolean;
begin
     Result := isReady;
end;

constructor THTMLDocBuilder.Create(name: string);
var
     check: boolean = TRUE;
begin
     filename := GetCurrentDirUTF8 + PathDelim + name + '.html';
     foldername := GetCurrentDirUTF8 + PathDelim + name + '_files';
     if FileExistsUTF8(filename) then
       check := DeleteFileUTF8(filename);
     if not DirectoryExistsUTF8(foldername) then
       check := CreateDirUTF8(foldername);
     Doc := TStringList.Create;
     isReady := check;
     isTableOpened := 0;
     ImgCount := 0;
     if isReady then StartDoc(name);
end;

destructor THTMLDocBuilder.Destroy;
begin
  if Assigned(Doc) then Doc.Free;
end;

end.

