unit testubit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, FramView, Forms, Controls, Graphics, Dialogs,
  ComCtrls, ExtCtrls;

const LoremIpsum = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, '+
                   'sed do eiusmod tempor incididunt ut labore et dolore magna '+
                   'aliqua. Ut enim ad minim veniam, quis nostrud exercitation '+
                   'ullamco laboris nisi ut aliquip ex ea commodo consequat. '+
                   'Duis aute irure dolor in reprehenderit in voluptate velit '+
                   'esse cillum dolore eu fugiat nulla pariatur. Excepteur sint '+
                   'occaecat cupidatat non proident, sunt in culpa qui officia '+
                   'deserunt mollit anim id est laborum.';

type

  { TForm1 }

  TForm1 = class(TForm)
    HTMLViewer: TFrameViewer;
    Panel1: TPanel;
    ToolBar: TToolBar;
    Start1Btn: TToolButton;
    procedure Start1BtnClick(Sender: TObject);
  public

  end;

var
  Form1: TForm1;

implementation

uses c_htmlbuilder, idocbuilder;

{$R *.lfm}

{ TForm1 }

procedure TForm1.Start1BtnClick(Sender: TObject);
var
  filename: string;
  HTMLDoc: THTMLDocBuilder;
  i: integer;
  JPEGimg1: TJPEGImage;
begin
   HTMLDoc := THTMLDocBuilder.Create('variant1');
     if HTMLDoc.Ready then
       begin
         HTMLDoc.AddHeader('Тест строителя HTML',[ffCenter]);
         HTMLDoc.AddText('Жирный абзац',[ffBold]);
         HTMLDoc.StartTable(['№','Название','Текст']);
         for i:=1 to 5 do
           HTMLDoc.AddRow([IntToStr(i),'бу-бу-бу',LoremIpsum]);
         HTMLDoc.AddRowHeader('заголовок в середине',[ffCenter,ffUnderlined]);
         for i:=5 downto 1 do
           HTMLDoc.AddRow([IntToStr(i),'бу-бу-бу','qqq']);
         HTMLDoc.FinishTable;
         HTMLDoc.AddHeader('Второй заголовок',[ffRight,ffBold,ffItalic]);
         HTMLDoc.AddText(LoremIpsum,[]);
         HTMLDoc.StartTable(['№ картинки','картинка']);
         JPEGimg1 := TJPEGImage.Create;
         JPEGimg1.LoadFromFile('test.jpg');
         HTMLDoc.AddRow(['1 картинка',JPEGimg1]);
         HTMLDoc.AddRowHeader('проверка меньшего количества',[]);
         HTMLDoc.AddRow(['1 столбец']);
         HTMLDoc.AddRow(['проверка большего количества','2 столбец','3 столбец']);
         HTMLDoc.AddRowHeader('добавление неопознанных объектов',[]);
         HTMLDoc.AddRow([1,ffBold]);
         HTMLDoc.FinishTable;
         JPEGimg1.Free;
         filename := HTMLDoc.FinishDoc;
         if filename<>'' then
           HTMLViewer.LoadFromFile(filename)
         else
           ShowMessage('Что-то пошло не так');
       end;
   HTMLDoc.Free;
end;

end.

